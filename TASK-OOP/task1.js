const Car = function (make, speed) {
  /**
   * Your code here (untuk property)
   * Your code here (untuk property)
   */
  this.make = make;
  this.speed = speed;
};

Car.prototype.accelerate = function () {
  /**
   * Your code here (untuk property)
   */
  this.speed +=5 

  console.log(`${this.make} is going at ${this.speed} km/h`);
};

Car.prototype.brake = function () {
  /**
   * Your code here (untuk property)
   */
  this.speed -=5
  console.log(`${this.make} is going at ${this.speed} km/h`);
};

/**
 * Inisiasi with keyword new
 * Your code here (untuk variable bmw & mercedes)
 */
const bmw = new Car("bmw",125);
const mercedes = new Car ("mercedes",100);

bmw.accelerate();
mercedes.accelerate();
bmw.brake();
mercedes.brake();

/**
 * Output: bmw is going 130
 * Output: mercedes is going 105
 * Output: bmw is going 125
 * Output: mercedes is going 100
 */
