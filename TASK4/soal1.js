// #1 Gunakan metode untuk membalikan isi array

let fruits = ["Apple", "Banana", "Papaya", "Grape", "Cherry", "Peach"];
fruits.reverse ();
console.log (fruits);
/* 
Output: 
Peach
Cherry
Grape
Papaya
Banana
Apple
*/