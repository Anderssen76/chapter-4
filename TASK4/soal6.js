/* #6 Menghitung BMI dengan if else statement
- John weights 95 kg and is 1.88 m tall. Nash weights 85 kg and is 1.76 
m tall

Output example:
John's BMI (28.3) is higher than Nash's (23.5)
*/
function bmi(mass,height){
    const bmi = mass / (height * height);
    return bmi;
    }
    const Johnmass = 95;
    const Johnheight= 1.88;
    const Johnbmi = bmi(Johnmass,Johnheight);

    
    const Nashmass = 85;
    const Nashheight= 1.76;
    const Nashbmi = bmi(Nashmass,Nashheight);


    if(Johnbmi>Nashbmi) {
        console.log("John's BMI (", Johnbmi,")", "is higher than", "Nash's (", Nashbmi, ")");

} else {
    console.log("Nash's BMI (", Nashbmi,")", "is higher than", "John's (", Johnbmi, ")");
}
    